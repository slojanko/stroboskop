# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

```
git clone https://slojanko@bitbucket.org/slojanko/stroboskop.git
```

Naloga 6.2.3:
https://bitbucket.org/slojanko/stroboskop/commits/251de2ea07515907e9e4ba84549ebf037190b328

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/slojanko/stroboskop/commits/5d6b7a9986fb5ada16f35907800326154235c6f5

Naloga 6.3.2:
https://bitbucket.org/slojanko/stroboskop/commits/7204263e75c7ae7e1e22677fc616795133b4887c

Naloga 6.3.3:
https://bitbucket.org/slojanko/stroboskop/commits/1d9e7fe87428e92b281d5f0890506bbb08689a36

Naloga 6.3.4:
https://bitbucket.org/slojanko/stroboskop/commits/172b6b9340f7b8f2eab1501f29e44874e696cdbf

Naloga 6.3.5:

```
git checkout master
git merge izgled
git push origin master
```

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/slojanko/stroboskop/commits/00e11a48aba3d67e7f9ca8373db4f8946724305d

Naloga 6.4.2:
https://bitbucket.org/slojanko/stroboskop/commits/65a11ba9f922dae4a56ae34218810e0060432942

Naloga 6.4.3:
https://bitbucket.org/slojanko/stroboskop/commits/58d367d4268760336a49b10f9d11da24602111e4

Naloga 6.4.4:
https://bitbucket.org/slojanko/stroboskop/commits/8445f1df35286d6119a2b4b19f887239d08fa33a